#pragma once

#include <stdbool.h>

struct cli_args {
    bool encrypt;
    char *codec;
};

struct cli_args cli_parse(int argc, char *argv[]);
