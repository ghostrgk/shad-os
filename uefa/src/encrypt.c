#include "encrypt.h"

void encrypt(char *data, size_t len)
{
    for (int i = 0; i < len; ++i) {
        int j = len - i - 1;
        if (i != j) {
            data[i] ^= data[j];
        }
    }
}

void decrypt(char *data, size_t len)
{
    encrypt(data, len);
    encrypt(data, len);
}
