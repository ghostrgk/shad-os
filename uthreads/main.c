#include "uthread.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

const unsigned long kArgF = 111;
const unsigned long kArgG = 222;
const int ans_pid_rep[] = {2, 1, 0};
int thread_pids[1000];
int cur_id = 0;

void g(void* data)
{
    assert(data == (void*)kArgG);

    static const int pid = 2;
    thread_pids[cur_id++] = pid;

    printf("In g(), data=%lx\n", (unsigned long) data);
}

void f(void* data)
{
    assert(data == (void*)kArgF);

    static const int pid = 1;
    thread_pids[cur_id++] = pid;
    for (int i = 0; i < 3; ++i) {
        printf("In f(), i=%d, data=%lx\n", i, (unsigned long) data);
        uthread_start(g, (void*)kArgG);
        uthread_yield();

        thread_pids[cur_id++] = pid;
    }
}

void check_pids()
{
    printf("cur_id = %d\n", cur_id);
    assert(cur_id == 12);
    for (int i = 0; i < cur_id; i++) {
        int rem = i % 3;
        if (rem == 0 && i == 0) {
            assert(thread_pids[i] == 0);
            continue;
        }
        assert(thread_pids[i] == ans_pid_rep[rem]);
    }
}

int main()
{
    static const int pid = 0;
    printf("In main()\n");
    uthread_start(f, (void*) kArgF);

    thread_pids[cur_id++] = pid;
    while (!uthread_try_join()) {
        printf("In main()\n");
        uthread_yield();
        thread_pids[cur_id++] = pid;
    }
    check_pids();
    return 0;
}
