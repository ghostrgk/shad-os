#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "backtrace.h"

void* bt0[1024];
int bt0_size;

void* bt1[1024];
int bt1_size;

void* bt2[1024];
int bt2_size;

void f0();
void f1();
void f2();
void f3();
void f4();
void f5();

void f0()
{
    f1();
    f2();
}

void f1()
{
    bt0_size = backtrace(bt0, 1024);
}

void f2()
{
    bt1_size = backtrace(bt1, 1024);
}

void f3()
{
    bt2_size = backtrace(bt2, 1024);
}

void f4()
{
    f3();
}

void f5()
{
    f4();
}

void test_backtrace()
{
    f0();
    f5();

#define CHECK_PTR(from, ptr, to) \
    fprintf(stderr, "checking: %s: 0x%x <= 0x%x <= 0x%x\n", #ptr, from, ptr, to); \
    assert((void*)from <= ptr && ptr <= (void*)to);

    CHECK_PTR(f1, bt0[0], f2);
    CHECK_PTR(f0, bt0[1], f1);

    CHECK_PTR(f2, bt1[0], f3);
    CHECK_PTR(f0, bt1[1], f1);

    CHECK_PTR(f3, bt2[0], f4);
    CHECK_PTR(f4, bt2[1], f5);
    CHECK_PTR(f5, bt2[2], test_backtrace);
}



void test_find_symbol()
{
    assert(!strcmp("f1", addr2name(bt0[0])));
    assert(!strcmp("f0", addr2name(bt0[1])));

    assert(!strcmp("f2", addr2name(bt1[0])));
    assert(!strcmp("f0", addr2name(bt1[1])));

    assert(!strcmp("f3", addr2name(bt2[0])));
    assert(!strcmp("f4", addr2name(bt2[1])));
    assert(!strcmp("f5", addr2name(bt2[2])));
}

void foo_bar()
{
    print_backtrace();
}

void dummy_frame()
{
    foo_bar();
}

void test_print_backtrace()
{
    dummy_frame();
}

int main(int argc, char *argv[])
{
    test_backtrace();
    test_find_symbol();
    test_print_backtrace();

    return 0;
}
