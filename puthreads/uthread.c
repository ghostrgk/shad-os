#include "uthread.h"
#include <assert.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>

// Large stack may be helpful!
#define UTHREAD_STACK_SIZE (4<<12)

// Signal frame on stack.
struct sigframe
{
    long r8, r9, r10, r11, r12, r13, r14, r15;
    long rdi, rsi, rbp, rbx, rdx, rax, rcx, rsp;
    long rip, flags;
};

// your code goes here.


void uthread_sched(int signum)
{
    // your code goes here.
}

void uthread_init()
{
    // Need to disable stdlib buffer to use printf from uthreads.
    setbuf(stdout, NULL);

    // Initialize timer.
    signal(SIGALRM, uthread_sched);
    ualarm(1000,1000);
}
